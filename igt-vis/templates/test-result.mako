<%
from datetime import datetime
import os
import re

re_pstore=re.compile("pstore%d-(.*)_[0-9]*.{log,txt}$" % test['run'])

filelist = [ ("..","integration-manifest.log"), ("..","git-log-oneline.log"),
             ("..","integration-manifest.txt"), ("..","git-log-oneline.txt") ]

if 'run' in test:
    filelist.append(("igt_runner%d.txt" % test['run'],))
    filelist.append(("run%d.txt" % test['run'],))
    filelist.append(("boot%d.txt" % test['run'],))
    filelist.append(("dmesg%d.txt" % test['run'],))
    filelist.append(("hostdmesg%d.txt" % test['run'],))
    filelist.append(("runtimes%d.txt" % test['run'],))

%>
<?xml version="1.0" encoding="UTF-8"?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>${title} Test details</title>
    <link rel="stylesheet" href="/assets/test_result.css" type="text/css" />
    <script type="text/javascript" src="/assets/test_result.js"></script>
    <link rel="shortcut icon" href="/assets/favicon.gif" />
  </head>
  <body>
    <h1>Results for ${testname}</h1>
    <div>
      <p><b>Result:</b> ${test['result']}</p>
    </div>
    <p>
    % for filetuple in filelist:
      % if os.path.isfile(os.path.join(path, *filetuple)):
        <a href="${'/'.join(filetuple)}">${os.path.splitext(filetuple[-1])[0]}</a>
      % endif
    % endfor
    </p>
    % for file in os.listdir(path):
      % if re.search(re_pstore, file):
        <a href="${file}">${os.path.splitext(file)[0]}</a>
      % endif
    % endfor
    </p>
    <table>
      <tr><th>Detail</th><th>Value</th></tr>
      % if 'time' in test:
      <tr><td>Duration</td><td>
      % if test['result'] == "incomplete":
        unknown
      % else:
        ${"%4.2f seconds" % (test['time']['end']-test['time']['start'])}
      % endif
      </b></tr>
      % endif
    % for var in ['hostname', 'returncode', 'igt-version', 'out', 'err', 'environment', 'command', 'exception', 'traceback', 'dmesg-warnings', 'dmesg']:
      % if var in test and test[var]:
      <tr><td>${var.title()}</td><td><pre>${test[var] | h}</pre></td></tr>
      % endif
    %endfor  
    </table>

    <i>Created at ${datetime.now().strftime('%Y-%m-%d %H:%M:%S')}</i>
  </body>
</html>
